if [ -r ~/.vim/bundle/fzf/shell/completion.zsh ]; then
    source ~/.vim/bundle/fzf/shell/completion.zsh
    source ~/.vim/bundle/fzf/shell/key-bindings.zsh
fi
